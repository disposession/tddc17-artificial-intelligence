;; In this test case, we will have 16 rooms, 2 boxes and 2 objects.
;; The goal will be to have object 1 in room 4, object 2 in room 16 and turning on the light in room 1, room 2,
;; room 3, room 4, room 5, room 6, room 7, room 8, room 9, room 10, room 11, room 12, room 13, room 14, room 15.

(define (problem pickup)
        (:domain shakeys)
        (:objects
            room1 room2 room3 room4 room5 room6 room7 room8 
	    room9 room10 room11 room12 room13 room14 room15 room16
            switch1 switch2 switch3 switch4 switch5 switch6 switch7 switch8
	    switch9 switch10 switch11 switch12 switch13 switch14 switch15 switch16
            box1 box2
            object1 object2
            arm1 arm2
        )

        (:init
            (room room1) (room room2) (room room3) (room room4) (room room5) (room room6) (room room7) (room room8) (room room9) (room room10) (room room11) (room room12) (room room13) (room room14) (room room15) (room room16)
            (switch switch1) (switch switch2) (switch switch3) (switch switch4) (switch switch5) (switch switch6) (switch switch7) (switch switch8) (switch switch9) (switch switch10) (switch switch11) (switch switch12) (switch switch13) (switch switch14) (switch switch15) (switch switch16)
            (switchfor switch1 room1) (switchfor switch2 room2) (switchfor switch3 room3) (switchfor switch4 room4) (switchfor switch5 room5) (switchfor switch6 room6) (switchfor switch7 room7) (switchfor switch8 room8) (switchfor switch9 room9) (switchfor switch10 room10) (switchfor switch11 room11) (switchfor switch12 room12) (switchfor switch13 room13) (switchfor switch14 room14) (switchfor switch15 room15) (switchfor switch16 room16)
            (box box1) (box box2) (object object1) (object object2)
            (arm arm1) (arm arm2)
            (widelyconnected room1 room2) (widelyconnected room2 room4) (widelyconnected room2 room3) (widelyconnected room3 room5) (widelyconnected room5 room6) (widelyconnected room6 room7) (widelyconnected room7 room11) (widelyconnected room11 room12) (widelyconnected room12 room13) (widelyconnected room13 room14)
            (connected room3 room4) (connected room15 room16) (connected room16 room14)
            (widelyconnected room5 room8) (widelyconnected room8 room9) (widelyconnected room9 room10) (widelyconnected room10 room15)
            
            (at object1 room16)
            (at object2 room15)
            (boxon box2 room15)
            (boxon box1 room14)
            (loc room1)
            (free arm1) (free arm2)
	    (lit room16)
        )
        
        (:goal (and (at object1 room4) (at object2 room16) (lit room1) (lit room2) (lit room3) (lit room4) (lit room5) (lit room6) (lit room7) (lit room8) (lit room9) (lit room10) (lit room11) (lit room12) (lit room13) (lit room14) (lit room15) ))
)
