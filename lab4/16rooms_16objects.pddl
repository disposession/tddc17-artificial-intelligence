;; In this test case, we will have 4 rooms, 1 box and 8 objects.
;; The goal will be to have object 1 in room 10, object 2 in room 16, object 3 in room 1, object 4 in room 1,
;; object 5 in room 14, object 6 in room 14, object 7 in room 9, object 8 in room 9, object 9 in room 1,
;; object 10 in room 9, object 11 in room 6, object 12 in room 16, object 13 in room 16, object 14 in room 10,
;; object 15 in room 4 and object 16 in room 12.


(define (problem pickup)
        (:domain shakeys)
        (:objects
            room1 room2 room3 room4 room5 room6 room7 room8 
	    room9 room10 room11 room12 room13 room14 room15 room16
            switch1 switch2 switch3 switch4 switch5 switch6 switch7 switch8
	    switch9 switch10 switch11 switch12 switch13 switch14 switch15 switch16
            box1 box2 box3 box4 box5 box6
            object1 object2 object3 object4 object5 object6 object7 object8 object9 object10 object11 object12 object13 object14 object15 object16 
            arm1 arm2
        )

        (:init
            (room room1) (room room2) (room room3) (room room4) (room room5) (room room6) (room room7) (room room8) (room room9) (room room10) (room room11) (room room12) (room room13) (room room14) (room room15) (room room16)
            (switch switch1) (switch switch2) (switch switch3) (switch switch4) (switch switch5) (switch switch6) (switch switch7) (switch switch8) (switch switch9) (switch switch10) (switch switch11) (switch switch12) (switch switch13) (switch switch14) (switch switch15) (switch switch16)
            (switchfor switch1 room1) (switchfor switch2 room2) (switchfor switch3 room3) (switchfor switch4 room4) (switchfor switch5 room5) (switchfor switch6 room6) (switchfor switch7 room7) (switchfor switch8 room8) (switchfor switch9 room9) (switchfor switch10 room10) (switchfor switch11 room11) (switchfor switch12 room12) (switchfor switch13 room13) (switchfor switch14 room14) (switchfor switch15 room15) (switchfor switch16 room16)
            (box box1) (box box2) (box box3) (box box4) (box box5) (box box6)
	    (object object1) (object object2) (object object3) (object object4) (object object5) (object object6) (object object7) (object object8) (object object9) (object object10) (object object11) (object object12) (object object13) (object object14) (object object15) (object object16)
            (arm arm1) (arm arm2)
            (widelyconnected room1 room2) (widelyconnected room2 room3) (widelyconnected room3 room4) (widelyconnected room5 room7) (widelyconnected room7 room8) (widelyconnected room8 room9) (widelyconnected room9 room10) (widelyconnected room12 room13) (widelyconnected room13 room15) (widelyconnected room15 room16)
            (connected room3 room5) (connected room5 room6) (connected room6 room4)
            (connected room8 room11) (connected room10 room12) (connected room11 room13) (connected room13 room14)
            
            (at object1 room1)
            (at object2 room8)
	    (at object3 room4)
            (at object4 room12)
	    (at object5 room10)
            (at object6 room4)
	    (at object7 room4)
            (at object8 room14)
	    (at object9 room15)
            (at object10 room15)
	    (at object11 room15)
            (at object12 room7)
	    (at object13 room2)
            (at object14 room16)
	    (at object15 room11)
            (at object16 room9)
            (boxon box1 room1)
            (boxon box2 room6)
	    (boxon box3 room9)
            (boxon box4 room11)
	    (boxon box5 room13)
            (boxon box6 room14)
            (loc room1)
            (free arm1) (free arm2)
        )
        
        (:goal (and (at object1 room10) (at object2 room16) (at object3 room1) (at object4 room1) (at object5 room14) (at object6 room14) (at object7 room9) (at object8 room9) (at object9 room1) (at object10 room9) (at object11 room6) (at object12 room16) (at object13 room16) (at object14 room10) (at object15 room4) (at object16 room12)))
)
