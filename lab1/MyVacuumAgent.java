package tddc17;


import aima.core.environment.liuvacuum.*;
import aima.core.agent.Action;
import aima.core.agent.AgentProgram;
import aima.core.agent.Percept;
import aima.core.agent.impl.*;

import java.util.Random;

class MyAgentState
{
	public int[][] world = new int[30][30];
	public int initialized = 0;
	final int UNKNOWN 	= 0;
	final int WALL 		= 1;
	final int CLEAR 	= 2;
	final int DIRT		= 3;
	final int HOME		= 4;
	final int ACTION_NONE 			= 0;
	final int ACTION_MOVE_FORWARD 	= 1;
	final int ACTION_TURN_RIGHT 	= 2;
	final int ACTION_TURN_LEFT 		= 3;
	final int ACTION_SUCK	 		= 4;
	
	public int agent_x_position = 1;
	public int agent_y_position = 1;
	public int agent_last_action = ACTION_NONE;
	// Current objective
	// 0 - Find wall
	// 1 - find corner
	// 2 - search tiles
	// 3 - Wall turn
	// 4 - Go home
	public int current_objective = 0;
	// if these are true, the actions will be performed no matter what
	public boolean turn = false;
	public boolean forward = false;
	// Check what was the last turn you made so you can switch this once
	public boolean left_turn = false;
	
	public static final int NORTH = 0;
	public static final int EAST = 1;
	public static final int SOUTH = 2;
	public static final int WEST = 3;
	public int agent_direction = EAST;
	
	MyAgentState()
	{
		for (int i=0; i < world.length; i++)
			for (int j=0; j < world[i].length ; j++)
				world[i][j] = UNKNOWN;
		world[1][1] = HOME;
		agent_last_action = ACTION_NONE;
	}
	// Based on the last action and the received percept updates the x & y agent position
	public void updatePosition(DynamicPercept p)
	{
		Boolean bump = (Boolean)p.getAttribute("bump");
		
		if (agent_last_action==ACTION_MOVE_FORWARD && !bump)
	    {
			switch (agent_direction) {
			case MyAgentState.NORTH:
				agent_y_position--;
				break;
			case MyAgentState.EAST:
				agent_x_position++;
				break;
			case MyAgentState.SOUTH:
				agent_y_position++;
				break;
			case MyAgentState.WEST:
				agent_x_position--;
				break;
			}
	    }
		
	}

	// This method encapsulates the necessary code to perform given action
	public Action performAction(int action){
		agent_last_action = action;
		if (action==ACTION_MOVE_FORWARD){
			return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
		}else if (action == ACTION_TURN_LEFT){
			agent_direction = (((agent_direction-1) + 4) % 4);
			return LIUVacuumEnvironment.ACTION_TURN_LEFT;
		}else if (action == ACTION_TURN_RIGHT){
			agent_direction = ((agent_direction+1) % 4);
			return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
		}else if (action == ACTION_SUCK){
			return LIUVacuumEnvironment.ACTION_SUCK;
		}else{
			return NoOpAction.NO_OP;
		}
	}
	
	public void updateWorld(int x_position, int y_position, int info)
	{
		world[x_position][y_position] = info;
	}
	
	public void printWorldDebug()
	{
		for (int i=0; i < world.length; i++)
		{
			for (int j=0; j < world[i].length ; j++)
			{
				if (world[j][i]==UNKNOWN)
					System.out.print(" ? ");
				if (world[j][i]==WALL)
					System.out.print(" # ");
				if (world[j][i]==CLEAR)
					System.out.print(" . ");
				if (world[j][i]==DIRT)
					System.out.print(" D ");
				if (world[j][i]==HOME)
					System.out.print(" H ");
			}
			System.out.println("");
		}
	}
}

class MyAgentProgram implements AgentProgram {

	private int initnialRandomActions = 10;
	private Random random_generator = new Random();
	
	// Here you can define your variables!
	public int iterationCounter = 10000;
	public MyAgentState state = new MyAgentState();
	
	// moves the Agent to a random start position
	// uses percepts to update the Agent position - only the position, other percepts are ignored
	// returns a random action
	private Action moveToRandomStartPosition(DynamicPercept percept) {
		int action = random_generator.nextInt(6);
		initnialRandomActions--;
		state.updatePosition(percept);
		if(action==0) {
		    state.agent_direction = (((state.agent_direction-1) +4) % 4);
		    state.agent_last_action = state.ACTION_TURN_LEFT;
			return LIUVacuumEnvironment.ACTION_TURN_LEFT;
		} else if (action==1) {
			state.agent_direction = ((state.agent_direction+1) % 4);
		    state.agent_last_action = state.ACTION_TURN_RIGHT;
		    return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
		} 
		state.agent_last_action=state.ACTION_MOVE_FORWARD;
		return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
	}
	
	@Override
	public Action execute(Percept percept) {
		// DO NOT REMOVE this if condition!!!
    	if (initnialRandomActions>0) {
    		return moveToRandomStartPosition((DynamicPercept) percept);
    	} else if (initnialRandomActions==0) {
    		// process percept for the last step of the initial random actions
    		initnialRandomActions--;
    		state.updatePosition((DynamicPercept) percept);
			System.out.println("Processing percepts after the last execution of moveToRandomStartPosition()");
			
			return state.performAction(state.ACTION_SUCK);
    	}
		
    	// This example agent program will update the internal agent state while only moving forward.
    	// START HERE - code below should be modified!
    	
	    iterationCounter--;
	    
	    // out of moves, go bye bye
	    if (iterationCounter==0)
	    	return state.performAction(state.ACTION_NONE);;

	    // Percept environment
	    DynamicPercept p = (DynamicPercept) percept;
	    Boolean bump = (Boolean)p.getAttribute("bump");
	    Boolean dirt = (Boolean)p.getAttribute("dirt");
	    Boolean home = (Boolean)p.getAttribute("home");
	    
	    // State update based on the percept value and the last action
	    System.out.println("Call update position");
	    state.updatePosition(p);
	    
	    System.out.println("x=" + state.agent_x_position);
    	System.out.println("y=" + state.agent_y_position);
    	System.out.println("dir=" + state.agent_direction);

	    // Update world map -- START
	    if (bump) {
			switch (state.agent_direction) {
			case MyAgentState.NORTH:
				state.updateWorld(state.agent_x_position,state.agent_y_position-1,state.WALL);
				break;
			case MyAgentState.EAST:
				state.updateWorld(state.agent_x_position+1,state.agent_y_position,state.WALL);
				break;
			case MyAgentState.SOUTH:
				state.updateWorld(state.agent_x_position,state.agent_y_position+1,state.WALL);
				break;
			case MyAgentState.WEST:
				state.updateWorld(state.agent_x_position-1,state.agent_y_position,state.WALL);
				break;
			}
	    }
	    if (dirt)
	    	state.updateWorld(state.agent_x_position,state.agent_y_position,state.DIRT);
	    else
	    	state.updateWorld(state.agent_x_position,state.agent_y_position,state.CLEAR);
	    
	    state.printWorldDebug();
	    // Update world map -- END

	    // Next action selection based on the percept value
	    // If there's dirt, no matter what, clean it off
	    if (dirt){
	    	return state.performAction(state.ACTION_SUCK);
	    }else{
	    	// Let's check what is our current objective and act accordingly.
	    	if(state.current_objective == 0){
	    		// Right now we don't know where we are, and we need to find a wall. 
	    		// Just walk forward until we bump.
	    		if (bump){
		    		// We bumped! Turn left and let's go to mission 1
		    		state.current_objective = 1;
		    		return state.performAction(state.ACTION_TURN_LEFT);
		    		//return NoOpAction.NO_OP;
		    	}else{
		    		return state.performAction(state.ACTION_MOVE_FORWARD);
		    	}
	    	}else if (state.current_objective == 1){
	    		// Stage 1 is looking for a corner. Just walk straight until you find one
	    		if (bump){
	    			// We're at a corner! Begin scanning! By facing away from the wall
		    		state.current_objective = 2;
		    		return state.performAction(state.ACTION_TURN_LEFT);
		    	}else{
		    		return state.performAction(state.ACTION_MOVE_FORWARD);
		    	}
	    	}else if (state.current_objective == 2){
	    		// This is where it starts to get tricky.
	    		// We'll keep going forward until we hit a wall.
	    		if (bump){
	    			// we hit the wall. Perform a 180º flip to the next lane
		    		state.current_objective = 3;
		    		if (!state.left_turn){
			    		state.forward = true;
			    		return state.performAction(state.ACTION_TURN_LEFT);
		    		}else{
			    		state.forward = true;
			    		return state.performAction(state.ACTION_TURN_RIGHT);
		    		}
		    	}else{
		    		return state.performAction(state.ACTION_MOVE_FORWARD);
		    	}
	    	}else if (state.current_objective == 3){
    			if (state.forward){
    				state.turn = true;
    				state.forward = false;
    				return state.performAction(state.ACTION_MOVE_FORWARD);
    			}else{
    				state.current_objective = 2;
    				if (bump){
    					state.current_objective = 4;
    					state.left_turn = true;
    					state.forward = false;
    					return state.performAction(state.ACTION_MOVE_FORWARD);
    				}
    				if (!state.left_turn){
			    		state.forward = true;
			    		state.left_turn = !state.left_turn;
			    		return state.performAction(state.ACTION_TURN_LEFT);
		    		}else{
			    		state.forward = true;
			    		state.left_turn = !state.left_turn;
			    		return state.performAction(state.ACTION_TURN_RIGHT);
		    		}
    			}
	    	}else if (state.current_objective == 4){
	    		// Every tile is checked. Go home.
	    		if (state.agent_x_position == 1 && state.agent_y_position == 1){
	    			return state.performAction(state.ACTION_NONE);
	    		}
	    		if (state.agent_direction != 0 && state.turn){
	    			return state.performAction(state.ACTION_TURN_LEFT);
	    		}else if (state.agent_direction == 0 && !bump){
	    			state.turn = false;
	    			state.forward = true;
		        	return state.performAction(state.ACTION_MOVE_FORWARD);
	    		}else if (state.agent_direction == 0 && bump){
	    			state.turn = false;
	    			state.forward = true;
		        	return state.performAction(state.ACTION_TURN_LEFT);
	    		}else if (state.forward){
	    			return state.performAction(state.ACTION_MOVE_FORWARD);
	    		}else{
	    			return state.performAction(state.ACTION_NONE);
	    		}
	    	}else{
	    		System.out.println("THIS IS NOT SUPPOSED TO BE HAPPENING");
	    		state.agent_last_action=state.ACTION_MOVE_FORWARD;
	    		return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
	    	}
	    	
	    }
	}
}

public class MyVacuumAgent extends AbstractAgent {
    public MyVacuumAgent() {
    	super(new MyAgentProgram());
	}
}
