;; In this test case, we will have 4 rooms, 1 box and 8 objects.
;; The goal will be to have object 3 in room 1, object 4 in room 2, object 5 in room 4, object 6 in room 5, 
;; object 1 in room 3, object 2 in room 3, object 7 in room 3 and object 8 in room 3


(define (problem pickup)
        (:domain shakeys)
        (:objects
            room1 room2 room3 room4 room5
            switch1 switch2 switch3 switch4 switch5
            box1 
            object1 object2 object3 object4 object5 object6 object7 object8
            arm1 arm2
        )

        (:init
            (room room1) (room room2) (room room3) (room room4) (room room5)
            (switch switch1) (switch switch2) (switch switch3) (switch switch4) (switch switch5)
            (switchfor switch1 room1) (switchfor switch2 room2) (switchfor switch3 room3) (switchfor switch4 room4) (switchfor switch5 room5)
            (box box1)
            (object object1) (object object2) (object object3) (object object4) (object object5) (object object6) (object object7) (object object8)
            (arm arm1) (arm arm2)
            (widelyconnected room2 room3) (widelyconnected room3 room4) (widelyconnected room4 room5) (widelyconnected room5 room1)
	    (connected room1 room2)

            (at object1 room1) (at object2 room2) (at object5 room3) (at object6 room3)
            (at object3 room3) (at object4 room3) (at object7 room4) (at object8 room5)
            (boxon box1 room3)
            (loc room2)
            (free arm1) (free arm2)
        )

        (:goal (and (at object3 room1) (at object4 room2) (at object5 room4) (at object6 room5) (at object1 room3) (at object2 room3) (at object7 room3) (at object8 room3)))
)

