;; Shakeys World

;; Our domain is composed of: rooms, boxes, objects, switches, doors and arms
;; Additionally, we include predicates to determine the several states/positions of each of the previous static predicates
;; Each of these is explained in detail below

(define (domain shakeys)
  (:requirements :strips)
  (:predicates 
	;; Static predicates:
	(room ?r) (box ?b) (object ?o) (switch ?s) (door ?d) (arm ?a)

	;; Non-static predicates
	;; r is the current room Shakey's on.
	(loc ?r) 
	;; The light of room r is lit
	(lit ?r)
	;; In what room is an unheld object currently
	(at ?o ?r) 
	;; Is Shakey holding object o
	(holding ?o) 
	;; What rooms have doors between them? connected means r1 and r2 have a door between them whilst widely connected means there is a wide door.
	(connected ?r1 ?r2) 
	(widelyconnected ?r1 ?r2) 
	;; Wheter the arm a is holding anything
	(free ?a) 
	;; Switch s turns room r's switch on
	(switchfor ?s ?r) 
	;; Box b is on room r
	(boxon ?b ?r)
   )

;; Our actions go as follows
;; pickup: To hold an object
;; To perform this action, we need shakey to be in the same room as the object is at, the light on that room to be lit and to have a free arm
;; If successful the picked up object will no longer be in the room, but instead being held. The used arm will no longer be free
  (:action 
	pickup
	:parameters(?o ?r ?a)
	:precondition (and (room ?r) (object ?o) (at ?o ?r) (loc ?r) (free ?a) (lit ?r))
	:effect (and (not (at ?o ?r)) (holding ?o) (not (free ?a)))
  )
;; drop: To place an object in a certain room
;; To perform this action, we need shakey to be holding an object, and to be in the same room where he wants to drop 
;; If successful the picked up object will no longer be in the room, but instead being held. The used arm will no longer be free
  (:action
        drop
        :parameters(?o ?r ?a)
        :precondition (and (object ?o) (room ?r) (arm ?a) (holding ?o) (loc ?r))
        :effect (and (at ?o ?r) (not (holding ?o)) (free ?a))
  )
;; gofromto: Move shakey from it's current room to an adjacent one.
;; This action can only occur if the destination room is either connected or widelyconnected to our current room. Notice that a door can get us from r1 to r2 or from r2 to r1, but the connected predicates are unilateral, so we must check if r1 is connected to r2 or r2 is connected to r1
;; When performed, Shakey's location is updated to be the destination room.
  (:action
	gofromto
	:parameters(?r1 ?r2)
	:precondition (and (room ?r1) (room ?r2) (loc ?r1) (or (or (connected ?r1 ?r2) (widelyconnected ?r1 ?r2)) (or (connected ?r2 ?r1) (widelyconnected ?r2 ?r1))))
	:effect (and (not (loc ?r1))  (loc ?r2))
  )
;; switchon: Shakey will turn on the light in the specified room using a box.
;; To perform this action, the used box must be in the same room as shakey's.
;; The result is having the specified room, lit.
  (:action
        switchon
        :parameters(?r ?s ?b)
        :precondition (and (loc ?r) (not (lit ?r)) (switchfor ?s ?r) (boxon ?b ?r))
        :effect (lit ?r)
  )
;; dragfromto: Shakey will move the box from one room to an adjacent one.
;; This action can only occur if shakey's location is the same as the box's. The current and destination rooms must have a wide door between them. That is to say, they must be widelyconnected.
;; When performed, shakey and the box will now be located at the destination room
  (:action
        dragfromto
        :parameters(?b ?r1 ?r2)
        :precondition (and (loc ?r1) (boxon ?b ?r1) (or (widelyconnected ?r1 ?r2) (widelyconnected ?r2 ?r1)))
        :effect (and (loc ?r2) (not (loc ?r1)) (not (boxon ?b ?r1)) (boxon ?b ?r2))
  )
)
