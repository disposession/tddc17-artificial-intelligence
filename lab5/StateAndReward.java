public class StateAndReward {

	
	/* State discretization function for the angle controller */
	public static String getStateAngle(double angle, double vx, double vy) {

		// We create the following states:
		// no angle, or velocity -> perfect
		// Use discretize to get finite number of states (e.g. 5)
		int badhula = discretize(angle, 5, -(Math.PI / 3.0), Math.PI / 3.0);
		
		return "state" + Integer.toString(badhula);
	}

	/* Reward function for the angle controller */
	public static double getRewardAngle(double angle, double vx, double vy) {
		
		// HERE COMES MIGHTY THOR
		if (angle == 0){
			return 100000.0;
		}
		return (Math.PI / 5)/Math.abs(angle)-1;
	}

	/* State discretization function for the full hover controller */
	public static String getStateHover(double angle, double vx, double vy) {

		

		int badhula = discretize(angle, 10, -(Math.PI / 3.0), Math.PI / 3.0);
		// int jenobra = discretize(vx, 5, -4, 4);
		int gahibu = discretize(vy, 5, -2, 2);
		return Integer.toString(badhula) + Integer.toString(gahibu);
	}

	/* Reward function for the full hover controller */
	public static double getRewardHover(double angle, double vx, double vy) {
		// Coefficient for speed and angle
		double speed_coefficient = 10;
		double angle_coefficient = 1;
		
		// BEHOLD HIS SUPERIOR HAMMER
		if (vy == 0){
			vy = 0.000001;
		}
		
		double speed_reward = 2.0/Math.abs(vy) - 1;
		return angle_coefficient * getRewardAngle(angle, vx, vy) + speed_coefficient * speed_reward;
		
	}

	// ///////////////////////////////////////////////////////////
	// discretize() performs a uniform discretization of the
	// value parameter.
	// It returns an integer between 0 and nrValues-1.
	// The min and max parameters are used to specify the interval
	// for the discretization.
	// If the value is lower than min, 0 is returned
	// If the value is higher than min, nrValues-1 is returned
	// otherwise a value between 1 and nrValues-2 is returned.
	//
	// Use discretize2() if you want a discretization method that does
	// not handle values lower than min and higher than max.
	// ///////////////////////////////////////////////////////////
	public static int discretize(double value, int nrValues, double min,
			double max) {
		if (nrValues < 2) {
			return 0;
		}

		double diff = max - min;

		if (value < min) {
			return 0;
		}
		if (value > max) {
			return nrValues - 1;
		}

		double tempValue = value - min;
		double ratio = tempValue / diff;

		return (int) (ratio * (nrValues - 2)) + 1;
	}

	// ///////////////////////////////////////////////////////////
	// discretize2() performs a uniform discretization of the
	// value parameter.
	// It returns an integer between 0 and nrValues-1.
	// The min and max parameters are used to specify the interval
	// for the discretization.
	// If the value is lower than min, 0 is returned
	// If the value is higher than max, nrValues-1 is returned
	// otherwise a value between 0 and nrValues-1 is returned.
	// ///////////////////////////////////////////////////////////
	public static int discretize2(double value, int nrValues, double min,
			double max) {
		double diff = max - min;

		if (value < min) {
			return 0;
		}
		if (value > max) {
			return nrValues - 1;
		}

		double tempValue = value - min;
		double ratio = tempValue / diff;

		return (int) (ratio * nrValues);
	}

}
