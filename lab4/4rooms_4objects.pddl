;; In this test case, we will have 4 rooms, 2 boxes and 4 objects.
;; The goal will be to have object 1 in room 4, object 2 in room 2, object 3 in room 3 and object 4 in room 3.


(define (problem pickup)
        (:domain shakeys)
        (:objects
            room1 room2 room3 room4
            switch1 switch2 switch3 switch4
            box1 box2
            object1 object2 object3 object4
            arm1 arm2
        )

        (:init
            (room room1) (room room2) (room room3) (room room4)
            (switch switch1) (switch switch2) (switch switch3) (switch switch4)
            (switchfor switch1 room1) (switchfor switch2 room2) (switchfor switch3 room3) (switchfor switch4 room4)
            (box box1) (box box2) 
	    (object object1) (object object2) (object object3) (object object4)
            (arm arm1) (arm arm2)
            (widelyconnected room1 room2)
            (connected room2 room3)
            (widelyconnected room1 room4) (widelyconnected room3 room4)

            (at object1 room1) (at object3 room1)
            (at object2 room1) (at object4 room1)
            (boxon box1 room3)
            (loc room2)
            (free arm1) (free arm2)
        )

        (:goal (and (at object1 room4) (at object2 room2) (at object3 room3) (at object4 room3)))
)


