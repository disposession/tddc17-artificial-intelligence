;; In this test case, we will have 8 rooms, 3 boxex and 2 objects.
;; The goal will be to have object 1 in room 8, object 2 in room 5 as well as turning on the light in room 1,
;; room 2, room 3, room 4, room 5, room 6, room 7 and room 8. 

(define (problem pickup)
        (:domain shakeys)
        (:objects
            room1 room2 room3 room4 room5 room6 room7 room8
            switch1 switch2 switch3 switch4 switch5 switch6 switch7 switch8
            box1 box2 box3
            object1 object2
            arm1 arm2
        )

        (:init
            (room room1) (room room2) (room room3) (room room4) (room room5) (room room6) (room room7) (room room8)
            (switch switch1) (switch switch2) (switch switch3) (switch switch4) (switch switch5) (switch switch6) (switch switch7) (switch switch8)
            (switchfor switch1 room1) (switchfor switch2 room2) (switchfor switch3 room3) (switchfor switch4 room4) (switchfor switch5 room5) (switchfor switch6 room6) (switchfor switch7 room7) (switchfor switch8 room8)
            (box box1) (box box2) (box box3) (object object1) (object object2)
            (arm arm1) (arm arm2)
            (widelyconnected room1 room5) (widelyconnected room5 room6) (widelyconnected room7 room8)
            (connected room1 room2)
            (widelyconnected room2 room3) (widelyconnected room3 room4) (widelyconnected room6 room7)

            (at object1 room4)
            (at object2 room7)
            (boxon box1 room3)
            (boxon box2 room7)
	    (boxon box3 room1)
            (loc room5)
            (free arm1) (free arm2)
        )

        (:goal (and (at object1 room8) (at object2 room5) (lit room1) (lit room2) (lit room3) (lit room4) (lit room5) (lit room6) (lit room7) (lit room8)))
)

