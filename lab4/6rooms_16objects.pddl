;; In this test case, we will have 6 rooms, 2 boxes and 16 objects.
;; The goal will be to have object 1 in  room 1, object 2 in room 1, object 5 in room 2, object 6 in room 2, 
;; object 3 in room 3, object 4 in room 3, object 7 in room 4, object 8 in room 5, object 9 in room 6, 
;; object 10 in room 6, object 11 in room 6, object 12 in room 6, object 13 in room 6, object 14 in room 6,
;; object 15 in room 6 and object 16 in room 6.


(define (problem pickup)
        (:domain shakeys)
        (:objects
            room1 room2 room3 room4 room5 room6
            switch1 switch2 switch3 switch4 switch5 switch6
            box1 box2
            object1 object2 object3 object4 object5 object6 object7 object8
	    object9 object10 object11 object12 object13 object14 object15 object16
            arm1 arm2
        )

        (:init
            (room room1) (room room2) (room room3) (room room4) (room room5) (room room6)
            (switch switch1) (switch switch2) (switch switch3) (switch switch4) (switch switch5) (switch switch6)
            (switchfor switch1 room1) (switchfor switch2 room2) (switchfor switch3 room3) (switchfor switch4 room4) (switchfor switch5 room5) (switchfor switch6 room6)
            (box box1) (box box2)
            (object object1) (object object2) (object object3) (object object4) (object object5) (object object6) (object object7) (object object8)
	    (object object9) (object object10) (object object11) (object object12) (object object13) (object object14) (object object15) (object object16)
            (arm arm1) (arm arm2)
            (widelyconnected room1 room2) (widelyconnected room1 room3) (widelyconnected room3 room6) (widelyconnected room4 room5)
            (connected room2 room4) (connected room3 room5)

            (at object1 room6) (at object2 room6) (at object5 room6) (at object6 room6)
            (at object3 room6) (at object4 room6) (at object7 room6) (at object8 room6)
	    (at object9 room1) (at object10 room1) (at object11 room2) (at object12 room2)
            (at object13 room3) (at object14 room3) (at object15 room4) (at object16 room5)

            (boxon box1 room6) (boxon box2 room5)
            (loc room2)
            (free arm1) (free arm2)
        )

        (:goal (and (at object1 room1) (at object2 room1) (at object5 room2) (at object6 room2) (at object3 room3) (at object4 room3) (at object7 room4) (at object8 room5) (at object9 room6) (at object10 room6) (at object11 room6) (at object12 room6) (at object13 room6) (at object14 room6) (at object15 room6) (at object16 room6)
))
)

